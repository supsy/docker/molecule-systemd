# docker-molecule-systemd

Docker image for Molecule Testing with Systemd

Run with:
```
docker run -it \
      --cap-add SYS_ADMIN \
      --tmpfs /tmp \
      --tmpfs /run \
      --tmpfs /run/lock \
      -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
      supsy/molecule-systemd:debian10
```
